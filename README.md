# Debian Preseed Generator

The goal of this project is to create a service that can be called with
specific given parameters to generate a [Preseed] file on the fly for use
with auto installation of [Debian]. The tech used is:

- A custom built [Althttpd] with changes for increased optimizations and
  support for the *TERM* signal when the process runs with a *PID* of `1`
  (which is the PID of the entrypoint in [Containers]).
- The [KCgi] C library.
- C using the C99 standard.

The resulting of building this application is a tiny image file that contains:

- `/`
  - `static-althttpd`
  - `www`
    - `preseed`
    - `post.bash`

All executables are compiled statically and with full optimizations. This means
that you could pull the image, extract its root, and run them outside a
container; however, it's simpler to just package into an image.

## Getting Started

The only dependencies required are:

- An [OCI] compliant container tool such as [Podman].
- A [POSIX] compliant shell.

To build the project:

1. Edit the [config.env] file to your liking.
2. Run the [build.sh] script.

To run the project, just run the [run.sh] script.

## Usage

The service is supposed to be used during the installation phase of Debian. To
use it:

1. Launch your system, VM, etc. with an installation ISO (or some other means
   of booting the installer).
2. When you see the boot menu, move the cursor down to the *Help* entry and
   press the *Enter* key.
3. At the boot prompt, type:
   `auto preseed/url=http://ip:port/preseed?arg1=val1&arg2=val2&...`. For
   example, say you have the *port* configured to `8000`, and your service is
   running on `10.0.2.2`. You want the machines options to be:

   - *hostname*: my-machine
   - *fullname*: Foo Bar
   - *username*: fbar

   You would pass the following prompt:
   `auto preseed/url=http://10.0.2.2:8000/preseed?hostname=my-machine&fullname=Foo%20Bar&username=fbar`.
   Note the `%20` in the *fullname* field. This represents a *space* character
   since url's can't contain spaces.

The machines will handle the rest.

**Warning**: If you use the default credentials, ensure you change them after
installation. Keep in mind that this services uses *http* and not *http***s**,
so if you pass any *password* arguments to the service, they are sent as plain
text. If you need to do this, I suggest placing a *http***s** proxy in front of
the service.

[Althttpd]: https://sqlite.org/althttpd/doc/trunk/althttpd.md "The Althttpd homepage."
[build.sh]: build.sh "The build script."
[config.env]: config.env "The config.env file."
[Containers]: https://opencontainers.org/ "The OCI homepage."
[Debian]: https://www.debian.org/ "The Debian homepage."
[KCgi]: https://kristaps.bsd.lv/kcgi/ "The KCgi C library homepage."
[OCI]: https://opencontainers.org/ "The OCI homepage."
[Podman]: https://podman.io/ "The Podman homepage."
[POSIX]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html "POSIX compliant shell specification."
[Preseed]: https://www.debian.org/releases/stable/amd64/apbs02.en.html "Preseeding documentation."
[run.sh]: run.sh "The run script."

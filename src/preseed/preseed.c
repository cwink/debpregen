#include <sys/types.h>

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <kcgi.h>

enum {
	FIELD_MAP_COL_LEN	= 3,
	PRESEED_TBL_COL_LEN	= 4,
};

enum field {
	FIELD_DISK		= 0,
	FIELD_FULLNAME,
	FIELD_HOST,
	FIELD_PROTO,
	FIELD_ROOTPASS,
	FIELD_USERNAME,
	FIELD_USERPASS,
	FIELD_MAX_VAL,
};

enum preseed {
	PRESEED_DISK_1		= 0,
	PRESEED_DISK_2,
	PRESEED_FULLNAME,
	PRESEED_HOST_1,
	PRESEED_HOST_2,
	PRESEED_PROTO,
	PRESEED_ROOTPASS_1,
	PRESEED_ROOTPASS_2,
	PRESEED_USERNAME,
	PRESEED_USERPASS_1,
	PRESEED_USERPASS_2,
	PRESEED_MAX_VAL,
};

#include "config.h"

static const char *const	page	= "preseed";
static struct kreq		request	= {0};

static void
exit_function(void)
{
	khttp_free(&request);
}

static void
print_preseed_table(void)
{
	const char	*k,	**se,	**sp,	*v;
	int		 i,	  len;

	k = kresps[KRESP_STATUS];
	v = khttps[KHTTP_200];
	khttp_head(&request, k, "%s", v);
	k = kresps[KRESP_CONTENT_TYPE];
	v = kmimetypes[KMIME_TEXT_PLAIN];
	khttp_head(&request, k, "%s", v);
	khttp_body(&request);
	len = sizeof(preseed_table) / sizeof(*preseed_table);
	for (i = 0; i < len; ++i) {
		sp = preseed_table[i];	
		se = sp + PRESEED_TBL_COL_LEN;
		khttp_printf(&request, "%s", *sp);
		for (++sp; sp != se; ++sp)
			khttp_printf(&request, "\t%s", *sp);
		khttp_printf(&request, "\n");
	}
}

static void
replace_preseed_vals(void)
{
	int		 *fp;
	struct kpair	**ke,	**kp;

	ke = request.fieldmap + request.keysz;
	for (kp = request.fieldmap; kp != ke; ++kp) {
		if (*kp == NULL)
			continue;
		for (fp = field_map[(*kp)->keypos]; *fp != -1; ++fp)
			preseed_table[*fp][3] = (*kp)->parsed.s;
	}
}

static void
validate_request(void)
{
	const char	 *k,	 *v;
	struct kpair	**ke,	**kp;
	
	ke = request.fieldnmap + request.keysz;
	for (kp = request.fieldnmap; kp != ke - 1; ++kp)
		if (*kp != NULL)
			break;
	if (*kp == NULL)
		return;
	k = kresps[KRESP_STATUS];
	v = khttps[KHTTP_400];
	khttp_head(&request, k, "%s", v);
	k = kresps[KRESP_CONTENT_TYPE];
	v = kmimetypes[KMIME_TEXT_PLAIN];
	khttp_head(&request, k, "%s", v);
	khttp_body(&request);
	v = fields[(*kp)->keypos].name;
	khttp_printf(&request, "invalid value(s) for field(s): %s", v);
	for (++kp; kp != ke; ++kp) {
		if (*kp == NULL)
			continue;
		v = fields[(*kp)->keypos].name;
		khttp_printf(&request, ", %s", v);
	}
	exit(EXIT_FAILURE);
}

int
main(void)
{
	int	rc;

	rc = khttp_parse(&request, fields, FIELD_MAX_VAL, &page, 1, 0);
	if (rc != KCGI_OK)
		return EXIT_FAILURE;
	atexit(exit_function);
	validate_request();
	replace_preseed_vals();
	print_preseed_table();
	return EXIT_SUCCESS;
}

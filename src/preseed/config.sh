#!/bin/sh

set -e

debug=0

warnings()
{
	(cat | tr '\r\n' ' ') <<-EOF
		-Wall
		-Walloc-zero
		-Wcast-qual
		-Wconversion
		-Wdouble-promotion
		-Wduplicated-branches
		-Wduplicated-cond
		-Wextra
		-Wfloat-equal
		-Wformat-signedness
		-Wformat=2
		-Winit-self
		-Wlogical-op
		-Wmissing-declarations
		-Wmissing-prototypes
		-Wpadded
		-Wshadow
		-Wsign-conversion
		-Wstrict-prototypes
		-Wswitch-default
		-Wswitch-enum
		-Wundef
		-Wunused-macros
		-Wwrite-strings
	EOF
}

cc='c99'
cflags='-D_POSIX_C_SOURCE=200809L -pedantic-errors'
cflags_debug='-fsanitize=undefined -fsanitize-trap -g -O0'
cflags_release='-Os'
kcflags="$(pkg-config --cflags kcgi)"
klibs="$(pkg-config --libs kcgi)"
ldflags='-static'
ldflags_debug=''
ldflags_release='-s -Wl,--gc-sections'
libs=''

cflags="${cflags} $(warnings)"

if [ "${debug}" -eq 1 ]; then
	cflags="${cflags} ${cflags_debug}"
	ldflags="${ldflags} ${ldflags_debug}"
else
	cflags="${cflags} ${cflags_release}"
	ldflags="${ldflags} ${ldflags_release}"
fi

cflags="${cflags} ${kcflags}"
libs="${libs} ${klibs}"

cat >'config.mk' <<-EOF
	CC	:= ${cc}
	CFLAGS	:= ${cflags}
	LDFLAGS	:= ${ldflags}
	LIBS	:= ${libs}
EOF

sed -i -e 's/:=\s\+/:= /' -e 's/\s\+$//' 'config.mk'

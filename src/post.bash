#!/bin/bash

set -e -o pipefail

host="${2}"
user="${1}"

apt-get --no-install-recommends -y install	\
	bash-completion				\
	ca-certificates				\
	crun					\
	curl					\
	dbus-user-session			\
	fuse-overlayfs				\
	man					\
	manpages				\
	podman					\
	rsync					\
	slirp4netns				\
	ssh					\
	sudo					\
	uidmap

mkdir -p "/home/${user}/.ssh"
touch "/home/${user}/.ssh/authorized_keys"
chown "${user}:${user}" "/home/${user}/.ssh"
chown "${user}:${user}" "/home/${user}/.ssh/authorized_keys"
chmod 0700 "/home/${user}/.ssh"
chmod 0600 "/home/${user}/.ssh/authorized_keys"

printf '%s\tALL=(ALL) NOPASSWD: ALL' "${user}" >"/etc/sudoers.d/${user}"

echo "${host}" >'/etc/hostname'

sed -i "s/debian/${host}/g" '/etc/hosts'

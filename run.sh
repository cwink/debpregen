#!/bin/sh

set -e

. './config.env'

exec podman run					\
	-p "${PORT}:${PORT}"			\
	--rm					\
	--userns 'keep-id'			\
	"${NAME}:${VERSION}"			\
	-port "${PORT}"

#!/bin/sh

set -e

. './config.env'

podman image rm -f "${NAME}:${VERSION}"

exec podman build			\
	--no-cache			\
	--squash			\
	-t "${NAME}:${VERSION}"		\
	'.'

#!/bin/sh

set -e

lint_docker()
{
	sed								\
		-e '/--mount=target=\/var\/cache\/apk,type=cache/d'	\
		-e 's/--mount=target=\/etc\/apk\/cache,type=cache//g'	|
		lint_with -i 'docker.io/hadolint/hadolint'
}

lint_markdown()
{
	lint_with 'ghcr.io/igorshubovych/markdownlint-cli:latest' "${@}"
}

lint_shell()
{
	lint_with 'docker.io/koalaman/shellcheck' -a -o all -x "${@}"
}

lint_with()
{
	podman run --rm -v "${PWD}:/mnt/pwd" -w '/mnt/pwd' "${@}"
}

lint_docker <'Containerfile'

lint_markdown 'README.md'

lint_shell			\
	'build.sh'		\
	'lint.sh'		\
	'run.sh'		\
	'src/post.bash'		\
	'src/preseed/config.sh'

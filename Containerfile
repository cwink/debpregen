FROM	docker.io/library/alpine:3.20 as alpine

COPY	["certs", "/usr/local/share/ca-certificates"]

WORKDIR	"/etc/apk/cache"

RUN	--mount=target=/etc/apk/cache,type=cache		   \
	--mount=target=/var/cache/apk,type=cache		   \
	sed -i 's/https:/http:/' '/etc/apk/repositories'	&& \
	apk update						&& \
	apk add "ca-certificates>=20240226"			&& \
	update-ca-certificates					&& \
	sed -i 's/http:/https:/' '/etc/apk/repositories'	&& \
	apk update

WORKDIR	"/"

FROM	alpine as build

RUN	--mount=target=/etc/apk/cache,type=cache	\
	--mount=target=/var/cache/apk,type=cache	\
	apk add						\
		'diffutils>=3.10'			\
		'fossil>=2.24'				\
		'gcc>=13.2.1_git20240309'		\
		'kcgi-dev>=0.13.3'			\
		'kcgi-static>=0.13.3'			\
		'make>=4.4.1'				\
		'musl-dev>=1.2.5'			\
		'patch>=2.7.6'				\
		'pkgconf>=2.2.0'			\
		'zlib-dev>=1.3.1'			\
		'zlib-static>=1.3.1'

FROM	build as althttpd

WORKDIR	"/tmp"

COPY	["src/althttpd.patch", "."]

RUN	fossil --user 'anonymous' clone 'https://sqlite.org/althttpd/'	\
		'althttpd.fossil'

WORKDIR	"/tmp/althttpd"

RUN	fossil open '/tmp/althttpd.fossil'				&& \
	fossil checkout '57952021f8'					&& \
	patch -p 1 <'/tmp/althttpd.patch'				&& \
	make static-althttpd

FROM	build as preseed

COPY	["src/preseed", "/tmp/preseed"]

WORKDIR	"/tmp/preseed"

RUN	./config.sh	&& \
	make

FROM	scratch as debpregen

WORKDIR	"/www"

COPY	--from=preseed ["/tmp/preseed/preseed", "."]
COPY	["src/post.bash", "."]

WORKDIR	"/"

COPY	--from=althttpd ["/tmp/althttpd/static-althttpd", "."]

ENTRYPOINT ["/static-althttpd", "-root", "/www"]
